// Inicializo materializer

M.AutoInit();
M.Modal.init()
// Inicializa Materialize CSS

//fincion que muestra y colapsa

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
});

// Inicializar modal
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);

    // Otras inicializaciones, como los selectores, se deben hacer aquí también.
});

