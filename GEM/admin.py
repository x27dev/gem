from django.contrib import admin
from .models import Oficina, Permiso, Empleado, JefeArea, Cargo, TipoPermiso

class OficinaAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'nombre')

class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'nombres', 'apellidos')

class CargoAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'nombre')

class JefeAreaAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'empleado', 'oficina')

class PermisoAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'empleado', 'tipo_permiso')

class TipoPermisoAdmin(admin.ModelAdmin):
    list_display = ('id', 'activo', 'nombre')


admin.site.register(Oficina,OficinaAdmin)

admin.site.register(Empleado,EmpleadoAdmin)

admin.site.register(Cargo,CargoAdmin)

admin.site.register(JefeArea,JefeAreaAdmin)

admin.site.register(Permiso,PermisoAdmin)
admin.site.register(TipoPermiso,TipoPermisoAdmin)
