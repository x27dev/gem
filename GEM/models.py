from django.contrib.auth.models import AbstractUser

from django.db import models

class Oficina(models.Model):
    nombre = models.CharField(max_length=100)
    activo = models.BooleanField(default=True)  # Agrega el campo "activo"

    def __str__(self):
        if self.activo:
            return self.nombre
        else:
            return f"({self.nombre} - Inactivo)"

    def delete(self, *args, **kwargs):
        # Cambiar el estado a inactivo en lugar de eliminar
        self.activo = False
        self.save()

        # Establecer los campos de relación a null
        self.empleado_set.update(oficina=None)
        self.jefearea_set.update(oficina=None)

class Cargo(models.Model):
    nombre = models.CharField(max_length=100)
    activo = models.BooleanField(default=True)  # Campo "activo"

    # Otros campos relacionados con el cargo

    def __str__(self):
        return self.nombre

class Empleado(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    direccion = models.CharField(max_length=200)
    telefono = models.CharField(max_length=15)
    dpi = models.CharField(max_length=20)
    nit = models.CharField(max_length=15)
    igss = models.CharField(max_length=15)
    oficina = models.ForeignKey(Oficina, on_delete=models.SET_NULL, null=True, blank=True)
    cargo = models.ForeignKey(Cargo, on_delete=models.SET_NULL, null=True, blank=True)
    banco = models.CharField(max_length=100)
    cuenta = models.CharField(max_length=50)
    sueldo = models.DecimalField(max_digits=10, decimal_places=2)
    bonificacion = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_inicio = models.DateField()
    fecha_finalizacion = models.DateField()
    no_contrato = models.CharField(max_length=20)
    renglon = models.CharField(max_length=20)
    activo = models.BooleanField(default=True)  # Campo "activo"

    def __str__(self):
        return f"{self.nombres} {self.apellidos}"

    def delete(self, *args, **kwargs):
        # Cambiar el estado a inactivo en lugar de eliminar
        self.activo = False
        self.save()

class JefeArea(models.Model):
    empleado = models.OneToOneField(Empleado, on_delete=models.SET_NULL, null=True, blank=True)
    oficina = models.ForeignKey(Oficina, on_delete=models.SET_NULL, null=True, blank=True)
    activo = models.BooleanField(default=True)  # Campo "activo"

    def __str__(self):
        return f"{self.empleado.nombres} ({self.empleado.cargo})"
    def delete(self, *args, **kwargs):
        # Cambiar el estado a inactivo en lugar de eliminar
        self.activo = False
        self.save()

class TipoPermiso(models.Model):
    nombre = models.CharField(max_length=100)
    activo = models.BooleanField(default=True)  # Campo "activo"

    # Otros campos relacionados con el cargo

    def __str__(self):
        return self.nombre

    def delete(self, *args, **kwargs):
        # Cambiar el estado a inactivo en lugar de eliminar
        self.activo = False
        self.save()

class Permiso(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.SET_NULL, null=True, blank=True)
    fecha_solicitud = models.DateField()
    fecha_inicio = models.DateField()
    fecha_regreso = models.DateField()
    tipo_permiso = models.ForeignKey(TipoPermiso, on_delete=models.SET_NULL, null=True, blank=True)
    justificacion = models.CharField(max_length=500)
    aprobado = models.BooleanField(default=False)

    activo = models.BooleanField(default=True)  # Campo "activo"

    def __str__(self):
        return f"{self.empleado.nombres} ({self.tipo_permiso})"
    def delete(self, *args, **kwargs):
        # Cambiar el estado a inactivo en lugar de eliminar
        self.activo = False
        self.save()

