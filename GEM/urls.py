from django.urls import path
from . import views

urlpatterns = [
    # Oficinas
    path('oficina/', views.oficina, name='oficina'),
    path('listar-oficinas/', views.listar_oficinas, name='listar_oficinas'),
    path('oficina/editar/<int:oficina_id>/', views.editar_oficina, name='editar_oficina'),
    path('oficina/eliminar/<int:oficina_id>/', views.eliminar_oficina, name='eliminar_oficina'),
    # Empleados
    path('empleados/', views.lista_empleados, name='lista-empleados'),
    path('crear_empleado/', views.crear_empleado, name='crear_empleado'),
    path('editar-empleado/<int:empleado_id>/', views.editar_empleado, name='editar_empleado'),
    path('eliminar-empleado/<int:empleado_id>/', views.eliminar_empleado, name='eliminar-empleado'),
    path('generar_reporte_empleados/', views.generar_reporte_empleados, name='generar_reporte_empleados'),
    # Tipos de Permisos
    path('tipo-permiso/', views.crear_editar_tipo_permiso, name='crear-tipo-permiso'),
    path('tipo-permiso/editar/<int:tipo_permiso_id>/', views.crear_editar_tipo_permiso, name='editar-tipo-permiso'),
    path('listar-tipos-permiso/', views.listar_tipos_permiso, name='listar-tipos-permiso'),
    path('tipo-permiso/eliminar/<int:tipo_permiso_id>/', views.eliminar_tipo_permiso, name='eliminar-tipo-permiso'),
    # Permisos
    path('permiso/editar/<int:permiso_id>/', views.crear_editar_permiso, name='editar-permiso'),
    path('permiso/crear/', views.crear_editar_permiso, name='crear-permiso'),
    path('permiso/listar/', views.listar_permisos, name='listar-permisos'),
    path('permiso/aprobar/<int:permiso_id>/', views.aprobar_permiso, name='aprobar-permiso'),

]
