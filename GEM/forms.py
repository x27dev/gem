from django import forms
from django.contrib.admin.widgets import AdminDateWidget

from .models import Empleado, Oficina, JefeArea, Permiso, Cargo, TipoPermiso


class OficinaForm(forms.ModelForm):
    class Meta:
        model = Oficina
        fields = '__all__'

class CargoForm(forms.ModelForm):
    class Meta:
        model = Cargo
        fields = '__all__'

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields = '__all__'  # Esto incluirá todos los campos del modelo Empleado en el formulario

    oficina = forms.ModelChoiceField(queryset=Oficina.objects.filter(activo=True), empty_label="Selecciona una oficina")
    cargo = forms.ModelChoiceField(queryset=Cargo.objects.filter(activo=True), empty_label="Selecciona un cargo")


class JefesForm(forms.ModelForm):
    class Meta:
        model = JefeArea
        fields = '__all__'

class TipoPermisoForm(forms.ModelForm):
    class Meta:
        model = TipoPermiso
        fields = '__all__'


class PermisoForm(forms.ModelForm):
    class Meta:
        model = Permiso
        fields = '__all__'
        exclude = ['activo']

    empleado = forms.ModelChoiceField(queryset=Empleado.objects.filter(activo=True), empty_label="Selecciona un empleado")
    tipo_permiso = forms.ModelChoiceField(queryset=TipoPermiso.objects.filter(activo=True), empty_label="Selecciona un tipo de permiso")
    fecha_solicitud = forms.DateField(widget=AdminDateWidget())
    fecha_inicio = forms.DateField(widget=AdminDateWidget())
    fecha_regreso = forms.DateField(widget=AdminDateWidget())