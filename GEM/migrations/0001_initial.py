# Generated by Django 4.2.5 on 2023-10-04 02:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=100)),
                ('apellidos', models.CharField(max_length=100)),
                ('direccion', models.CharField(max_length=200)),
                ('telefono', models.CharField(max_length=15)),
                ('dpi', models.CharField(max_length=20)),
                ('nit', models.CharField(max_length=15)),
                ('igss', models.CharField(max_length=15)),
                ('cargo', models.CharField(max_length=100)),
                ('banco', models.CharField(max_length=100)),
                ('cuenta', models.CharField(max_length=50)),
                ('sueldo', models.DecimalField(decimal_places=2, max_digits=10)),
                ('bonificacion', models.DecimalField(decimal_places=2, max_digits=10)),
                ('fecha_inicio', models.DateField()),
                ('fecha_finalizacion', models.DateField()),
                ('no_contrato', models.CharField(max_length=20)),
                ('renglon', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Oficina',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Permiso',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_solicitud', models.DateField()),
                ('fecha_inicio', models.DateField()),
                ('fecha_regreso', models.DateField()),
                ('tipo_permiso', models.CharField(max_length=100)),
                ('justificacion', models.CharField(max_length=500)),
                ('empleado', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='GEM.empleado')),
            ],
        ),
        migrations.CreateModel(
            name='JefeArea',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('empleado', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='GEM.empleado')),
                ('oficina', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='GEM.oficina')),
            ],
        ),
        migrations.AddField(
            model_name='empleado',
            name='oficina',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='GEM.oficina'),
        ),
    ]
