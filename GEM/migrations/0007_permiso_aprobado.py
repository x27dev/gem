# Generated by Django 4.2.5 on 2023-10-12 22:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GEM', '0006_alter_permiso_empleado_alter_permiso_tipo_permiso'),
    ]

    operations = [
        migrations.AddField(
            model_name='permiso',
            name='aprobado',
            field=models.BooleanField(default=False),
        ),
    ]
