# Generated by Django 4.2.5 on 2023-10-12 22:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('GEM', '0005_tipopermiso'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permiso',
            name='empleado',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='GEM.empleado'),
        ),
        migrations.AlterField(
            model_name='permiso',
            name='tipo_permiso',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='GEM.tipopermiso'),
        ),
    ]
