# GEM/views.py
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from reportlab.lib import colors

from .models import Oficina, Empleado, Cargo, TipoPermiso, Permiso
from .forms import OficinaForm, EmpleadoForm, TipoPermisoForm, PermisoForm

#reportes
from reportlab.lib.pagesizes import landscape, letter, A4
from .models import Empleado
from django.http import HttpResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle, PageBreak
from io import BytesIO

# ------------------------- Oficina
def oficina(request):
    if request.method == 'POST':
        form = OficinaForm(request.POST)
        if form.is_valid():
            # Crear una nueva instancia de oficina con activo=True antes de guardar
            nueva_oficina = form.save(commit=False)
            nueva_oficina.activo = True
            nueva_oficina.save()
            return redirect('oficina')

    else:
        form = OficinaForm()

    oficinas = Oficina.objects.filter(activo=True)

    context = {
        'oficinas': oficinas,
        'form': form,
    }

    return render(request, 'GEM/Oficina/oficina.html', context)

def listar_oficinas(request):
    oficinas = Oficina.objects.filter(activo=True)
    oficinas_con_empleados = []

    for oficina in oficinas:
        empleados = Empleado.objects.filter(oficina=oficina)
        oficinas_con_empleados.append({
            'oficina': oficina,
            'empleados': empleados
        })

    return render(request, 'GEM/Oficina/oficina-empleados.html', {'oficinas_con_empleados': oficinas_con_empleados})

def editar_oficina(request, oficina_id):
    # Obtén la oficina que deseas editar
    oficina = Oficina.objects.get(id=oficina_id)

    if request.method == 'POST':
        # Si se envió un formulario, procesa los datos
        form = OficinaForm(request.POST, instance=oficina)
        if form.is_valid():
            form.save()
            return redirect('oficina')  # Redirige a la lista de oficinas o a donde desees
    else:
        # Si no se envió un formulario, crea uno con los datos de la oficina
        form = OficinaForm(instance=oficina)

    return render(request, 'GEM/Oficina/editar_oficina.html', {'form': form})

def eliminar_oficina(request, oficina_id):
    try:
        oficina = Oficina.objects.get(id=oficina_id)
        oficina.delete()  # Llama al método delete personalizado
    except Oficina.DoesNotExist:
        # Puedes manejar aquí la situación en la que la oficina no se encuentra
        pass

    return redirect('oficina')  # Redirige a la lista de oficinas o a donde desees


# ------------------------- Empleado
def lista_empleados(request):
    empleados = Empleado.objects.filter(activo=True)
    form = EmpleadoForm()

    search_query = request.GET.get('search')  # Obtener el valor de búsqueda

    if search_query:
        empleados = empleados.filter(Q(nombres__icontains=search_query) | Q(oficina__nombre__icontains=search_query))

    if request.method == 'POST':
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista-empleados')

    context = {
        'empleados': empleados,
        'form': form,
    }

    return render(request, 'GEM/Empleado/lista_empleados.html', context)

def crear_empleado(request):
    if request.method == 'POST':
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.activo = True
            form.save()
            return redirect('lista-empleados')
    else:
        form = EmpleadoForm()

    # Asegúrate de que oficinas se esté pasando al contexto
    oficinas = Oficina.objects.filter(activo=True)
    cargos = Cargo.objects.filter(activo=True)
    return render(request, 'GEM/Empleado/crear_empleado.html', {'form': form, 'oficinas': oficinas, 'cargos': cargos})

def generar_reporte_empleados(request):
    # Recupera la lista de empleados activos
    empleados = Empleado.objects.filter(activo=True)

    # Crea un objeto HttpResponse con el encabezado del PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="reporte_empleados.pdf"'
    buffer = BytesIO()
    doc = SimpleDocTemplate(buffer, pagesize=landscape(letter), leftMargin=10, rightMargin=10, topMargin=40, bottomMargin=40)
    elementos = []

    for empleado in empleados:
        # Crea una tabla con dos filas para mostrar la información de cada empleado
        datos_empleado = [
            ["Nombres: ", f"{empleado.nombres} {empleado.apellidos}", "DPI: ", empleado.dpi],
            ["Teléfono: ", empleado.telefono, "Dirección: ", empleado.direccion],
            ["NIT: ", empleado.nit, "IGSS: ", empleado.igss],
            ["Banco: ", empleado.banco, "Cuenta: ", empleado.cuenta],
            ["Sueldo: ", f"Q.{empleado.sueldo}", "Bonificación: ", f"Q.{empleado.bonificacion}"],
            ["Fecha de Inicio:",  f"{empleado.fecha_inicio}", "Fecha de Finalización:", f" {empleado.fecha_finalizacion}"],
            ["Número de Contrato: ", empleado.no_contrato, "Renglón: ", empleado.renglon],
        ]

        tabla = Table(datos_empleado, colWidths=[150, 200, 150, 200])
        tabla.setStyle(TableStyle([
            ('BACKGROUND', (0, 0), (-1, 0), (0.8, 0.8, 0.8)),
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('FONTSIZE', (0, 0), (-1, -1), 12),  # Aplicar el tamaño de fuente personalizado

        ]))

        elementos.append(tabla)
    doc.build(elementos)
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def editar_empleado(request, empleado_id):
    # Obtén el empleado que deseas editar
    empleado = Empleado.objects.get(id=empleado_id)

    if request.method == 'POST':
        form = EmpleadoForm(request.POST, instance=empleado)
        if form.is_valid():
            # Antes de guardar, obtén el valor actual de 'activo' y configúralo en el formulario
            form.instance.activo = True
            form.save()
            return redirect('lista-empleados')
    else:
        form = EmpleadoForm(instance=empleado)

    return render(request, 'GEM/Empleado/editar_empleado.html', {'form': form})


def eliminar_empleado(request, empleado_id):
    try:
        empleado = Empleado.objects.get(id=empleado_id)
        empleado.delete()
    except Empleado.DoesNotExist:
        pass

    return redirect('lista-empleados')


# ------------------------- tipos-permiso


def crear_editar_tipo_permiso(request, tipo_permiso_id=None):
    # Si se proporciona un 'tipo_permiso_id', estamos editando un objeto existente
    if tipo_permiso_id:
        tipo_permiso = TipoPermiso.objects.get(pk=tipo_permiso_id)
    else:
        tipo_permiso = None

    if request.method == 'POST':
        form = TipoPermisoForm(request.POST, instance=tipo_permiso)
        if form.is_valid():
            form.save()
            return redirect('listar-tipos-permiso')  # Redirige a la lista de tipos de permiso
    else:
        form = TipoPermisoForm(instance=tipo_permiso)

    return render(request, 'GEM/permisos/crear_editar_tipo_permiso.html', {'form': form, 'tipo_permiso': tipo_permiso})

def listar_tipos_permiso(request):
    tipos_permiso = TipoPermiso.objects.filter(activo=True)
    return render(request, 'GEM/Permisos/listar_tipos_permiso.html', {'tipos_permiso': tipos_permiso})

def eliminar_tipo_permiso(request, tipo_permiso_id):
    tipo_permiso = get_object_or_404(TipoPermiso, id=tipo_permiso_id)
    tipo_permiso.delete()  # Utiliza el método de eliminación personalizado

    return redirect('listar-tipos-permiso')  # Redirige a la lista de tipos de permiso o a donde desees


# ------------------------- permiso


def crear_editar_permiso(request, permiso_id=None):
    if permiso_id:
        permiso = get_object_or_404(Permiso, id=permiso_id)
        titulo = "Editar Permiso"
    else:
        permiso = None
        titulo = "Crear Permiso"

    if request.method == 'POST':
        form = PermisoForm(request.POST, instance=permiso)
        if form.is_valid():
            form.save()
            return redirect('listar-permisos')
    else:
        form = PermisoForm(instance=permiso)

    context = {'form': form, 'titulo': titulo}
    return render(request, 'GEM/pERMISOS/crear_editar_permiso.html', context)


def listar_permisos(request):
    empleados_con_permisos = Empleado.objects.filter(permiso__activo=True).distinct()
    permisos_por_empleado = []
    search_query = request.GET.get('search', '')

    if search_query:
        # Filtrar empleados por nombre
        empleados_con_permisos = empleados_con_permisos.filter(
            Q(nombres__icontains=search_query) | Q(apellidos__icontains=search_query))

    for empleado in empleados_con_permisos:
        permisos = Permiso.objects.filter(empleado=empleado, activo=True)
        permisos_por_empleado.append({
            'empleado': empleado,
            'permisos': permisos
        })

    return render(request, 'GEM/Permisos/listar_permisos.html', {'permisos_por_empleado': permisos_por_empleado, 'search_query': search_query})



def aprobar_permiso(request, permiso_id):
    permiso = get_object_or_404(Permiso, id=permiso_id)

    # Verificar el estado actual de aprobación
    if not permiso.aprobado:
        # Si no está aprobado, aprobarlo
        permiso.aprobado = True
    else:
        # Si ya está aprobado, revocar la aprobación
        permiso.aprobado = False

    permiso.save()

    return redirect('listar-permisos')